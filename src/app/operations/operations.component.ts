import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { OperationService } from './service/operation.service';
import { transaction } from './transaction';

@Component({
  selector: 'app-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.css']
})
export class OperationsComponent implements OnInit {
  operations : String[]=
  ['virement','versement','prelevement']
  transaction:transaction=new transaction();
  message:any;
  ch:string;

  constructor(private service:OperationService,private snackBar: MatSnackBar,private dialog: MatDialog, private toastr : ToastrService) { }

  ngOnInit(): void {

   
  }

  virement()
  {
    let resp= this.service.virement(this.transaction);
    resp.subscribe((data)=>this.message=data);
    console.log(this.message);
    this.toastr.success("problem");

  }

  prelevement()
  {
    let resp= this.service.prelevement(this.transaction);
    resp.subscribe((data)=>this.message=data);
    console.log(this.message);
    this.confirmer(this.message);
    //if(this.message=='done')
    //{console.log("hafeliii");}
  }

  confirmer(ch: string)
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose =false;
    dialogConfig.autoFocus = true;   
    dialogConfig.data = { ch};
    dialogConfig.width = "60%";
    this.dialog.open(ConfirmationComponent,dialogConfig);
    
  }

  versement()
  {
    let resp= this.service.versement(this.transaction);
    resp.subscribe((data)=>this.message=data);
   
    console.log(this.message);
  }
}
