import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { personne_physique } from 'app/client/personne_physique';
import { PersonnePhysiqueService } from 'app/service_clients/personne-physique.service';
import {pays} from 'app/client/pays';
import { FormControl, Validators } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { RevenuComponent } from './revenu/revenu.component';
import { NationaliteComponent } from './nationalite/nationalite.component';
import { revenu } from 'app/client/revenu';
import { RevenuService } from 'app/service_clients/revenu.service';
import { PepComponent } from './pep/pep.component';
import { liste_nat } from 'app/client/list_nat';
import { ParenteComponent } from './parente/parente.component';
import { listeSecteur } from 'app/client/listeSecteur';
import { listeProfession } from 'app/client/listeProfession';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
 
  message:any;
   message1:any;
   personne_physique:personne_physique=new personne_physique();
   revenu :revenu =new revenu();
   v:number;
   liste_nationnalite=liste_nat;
  test: Boolean;
  listeSecteur=listeSecteur;
  listeProfession=listeProfession;
  
  
  constructor(private service:PersonnePhysiqueService, private router: Router ,private snackBar: MatSnackBar,private dialog: MatDialog) { }


  
  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
   
  ngOnInit(): void {
  }

  public CreateNow()
  {
    
      
    let resp= this.service.updatePP(this.personne_physique.codeClient,this.personne_physique);
    resp.subscribe((data)=>this.message=data);
    
    let snackBarRef = this.snackBar.open('Client cree!', 'Bravo', {
      duration: 3000
    });

    this.router.navigate(['table-list']);
   
  }
     


      onClick(Code_clt:number) {  
      const dialogConfig = new MatDialogConfig();
       dialogConfig.disableClose = true;
       dialogConfig.autoFocus = true;   
       dialogConfig.data = { Code_clt };
       dialogConfig.width = "60%";
       this.dialog.open(RevenuComponent,dialogConfig);
      } 


       onClick1(Code_clt:number) {  
       const dialogConfig = new MatDialogConfig();
       dialogConfig.disableClose = true;
       dialogConfig.autoFocus = true;   
       dialogConfig.data = { Code_clt };
       dialogConfig.width = "60%";
       this.dialog.open(PepComponent,dialogConfig);
      } 

      onClick2(Code_clt:number,P:personne_physique) {  
        this.personne_physique.numCin=Code_clt;
        let resp= this.service.CreatePP(this.personne_physique);
        resp.subscribe((data)=>this.message=data);
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;   
        dialogConfig.data = { Code_clt,P };
        dialogConfig.width = "60%";
        this.dialog.open(NationaliteComponent,dialogConfig);
      
      
      }

      
     onClick3(Code_clt:number) {  
      let resp= this.service.CreatePP(this.personne_physique);
      resp.subscribe((data)=>this.message=data);
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;   
      dialogConfig.data = { Code_clt };
      dialogConfig.width = "60%";
      this.dialog.open(ParenteComponent,dialogConfig);
    
      }

}
